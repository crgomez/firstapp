-- MySQL dump 10.13  Distrib 8.0.23, for Linux (x86_64)
--
-- Host: localhost    Database: school
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clasification`
--

DROP TABLE IF EXISTS `clasification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clasification` (
  `id` varchar(6) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasification`
--

LOCK TABLES `clasification` WRITE;
/*!40000 ALTER TABLE `clasification` DISABLE KEYS */;
INSERT INTO `clasification` VALUES ('ADMI','ADMINISTRADOR DEL SISTEMA'),('ALUM','ALUMNO INSCRITO'),('DOCE','DOCENTE TIEMPO COMPLETO'),('INGLES','DOCENTE DE TIEMPO COMPLETO DE INGLES');
/*!40000 ALTER TABLE `clasification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES ('IGC001','Ingeniería Civil','Relacionado con estructuras de concreto, hierro, asi como construcción de caminos, carreteras.'),('MECA002','Mecatrónica','Se enfoca al desarrollo de robots y circuitos'),('MERCA001','Mercadotecnia','Se enfoca principalmente en la creacion de marcas, estudio de mercado y lo que conlleva a la administracion de una empresa'),('TIC001','Tecnologías de la Información','Se enfoca en ciertas areas de la informática como programación y redes');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_information`
--

DROP TABLE IF EXISTS `personal_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_information` (
  `matricule` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `age` int NOT NULL,
  `country` varchar(150) NOT NULL,
  `clasification` varchar(6) NOT NULL,
  PRIMARY KEY (`matricule`),
  KEY `clasification` (`clasification`),
  CONSTRAINT `personal_information_ibfk_1` FOREIGN KEY (`clasification`) REFERENCES `clasification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_information`
--

LOCK TABLES `personal_information` WRITE;
/*!40000 ALTER TABLE `personal_information` DISABLE KEYS */;
INSERT INTO `personal_information` VALUES ('0431876560','Francisco','Hernández Pérez',24,'España','ALUM'),('0715463829','Guts','Hfu',65,'X','ALUM'),('0731859754','Jairo','Martínez Hernández',22,'México','ALUM'),('0735816972','Estela','Hernández Rivera',19,'México','ALUM'),('0754696809','Maria','Juárez Vite',24,'España','ALUM'),('0765645831','Cira','García Salas',21,'México','ALUM'),('0937003742','Macario','Alvarado Hernandez',23,'Mexico','ALUM'),('0973554876','Epifanio Eustaquio','Reyes Valdivia',24,'Ecuador','ALUM'),('2154873198','Pedro','Hernandez Alonzo',25,'España','ALUM'),('2194739254','Cristian','Gomez Garcia',23,'Mexico','DOCE'),('3187624579','Ximena','Juarez Rodríguez',24,'México','ALUM'),('3781824919','Maciel','Hernández Perez',21,'México','ALUM'),('5708456459','Maria de los Ángeles','Hernández Hernández',24,'México','ALUM'),('6622576588','Olivia','Hernández Hernández',22,'Mexico','ALUM'),('7352839571','Estela','Juarez Reyes',21,'Mexico','ALUM'),('7618548864','Leonel','Gonzalez Hernández',23,'Argentina','ALUM'),('9186488540','María','Fuentes Hernández',24,'México','ALUM'),('9438782065','Surelia','Hernández Hernández',23,'México','ALUM'),('9576579057','Laura','Perales Olivares',21,'Argentina','ALUM'),('9738816827','Juana','Ramírez Martínez',26,'Colombia','ALUM'),('9751983507','Luis Enrique','Cruz Hernández',22,'México','ALUM');
/*!40000 ALTER TABLE `personal_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `matricule` varchar(10) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `session_type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matricule_UNIQUE` (`matricule`),
  KEY `session_type` (`session_type`),
  CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`matricule`) REFERENCES `personal_information` (`matricule`),
  CONSTRAINT `sessions_ibfk_2` FOREIGN KEY (`session_type`) REFERENCES `sessions_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES (2,'2194739254','ad7694f289bd796e9cb02769f6584f12','2021-01-19 13:35:39','DOCENTE001');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions_type`
--

DROP TABLE IF EXISTS `sessions_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions_type` (
  `id` varchar(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions_type`
--

LOCK TABLES `sessions_type` WRITE;
/*!40000 ALTER TABLE `sessions_type` DISABLE KEYS */;
INSERT INTO `sessions_type` VALUES ('ALUMNO0001','ALUMNO INSCRITO','ALUMNO PERTENECIENTE A ESTA INSTITUCION EDUCATIVA'),('ALUMNO0002','ALUMNO EGRESADO','ALUMNO QUE FUE PERTENECIENTE A ESTA INSTITUCION EDUCATIVA'),('DOCENTE001','DOCENTE TC','DOCENTE DE TIEMPO COMPLETO'),('DOCENTE002','DOCENTE TP','DOCENTE DE TIEMPO PARCIAL'),('DOCENTE003','DOCENTE LE','DOCENTE DE LENGUA EXTRANJERA');
/*!40000 ALTER TABLE `sessions_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_has_courses`
--

DROP TABLE IF EXISTS `students_has_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students_has_courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `matricule` varchar(10) NOT NULL,
  `course` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `matricule_UNIQUE` (`matricule`),
  KEY `course` (`course`),
  CONSTRAINT `students_has_courses_ibfk_1` FOREIGN KEY (`matricule`) REFERENCES `personal_information` (`matricule`),
  CONSTRAINT `students_has_courses_ibfk_2` FOREIGN KEY (`course`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_has_courses`
--

LOCK TABLES `students_has_courses` WRITE;
/*!40000 ALTER TABLE `students_has_courses` DISABLE KEYS */;
INSERT INTO `students_has_courses` VALUES (146,'0731859754','MECA002'),(147,'0765645831','MECA002'),(148,'7352839571','MECA002'),(149,'7618548864','MECA002'),(150,'3187624579','MECA002'),(151,'0973554876','MECA002'),(152,'9751983507','TIC001'),(153,'0937003742','TIC001'),(154,'9186488540','TIC001'),(155,'0735816972','IGC001'),(156,'9738816827','IGC001'),(157,'2154873198','MERCA001'),(158,'3781824919','MERCA001'),(159,'9438782065','MERCA001'),(160,'0431876560','TIC001'),(161,'6622576588','TIC001'),(162,'9576579057','IGC001'),(163,'5708456459','IGC001'),(164,'0754696809','MERCA001');
/*!40000 ALTER TABLE `students_has_courses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-04 14:44:12
