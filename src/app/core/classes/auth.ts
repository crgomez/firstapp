export class AuthClass {
    validSession(Data: any): String {
        let url: String;
        /* Evalua si la sesión ha sido aprobada o no; el api retorna un true o false en la variable success,
        dependiendo si los datos enviados como parámetros coinciden o no con los de la tabla correspondiente */
        if (Data.data.success) {
            // Si es true retornará la url que redireccionará al home
            url = '/home';
        } else {
            // Si es false retornará la url que redireccionará al login nuevamente para introducir sus credenciales de nuevo
            url = '/login';
        }
        return url;
    }
}
