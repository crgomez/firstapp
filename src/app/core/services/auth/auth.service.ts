import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@src/environments/environment';
import { ApplicationSettings } from '@nativescript/core';
import { AuthClass } from '@src/app/core/classes/auth';
import { RouterExtensions } from '@nativescript/angular';
import { AppSession } from '@src/app/core/models/appsession';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authClass: AuthClass = new AuthClass();

  constructor(
    private http: HttpClient,
    private routerExtensions: RouterExtensions
  ) {
  }

  hasSessionActive() {
    if (ApplicationSettings.getString('data')) {
      const data = JSON.parse(ApplicationSettings.getString('data'));
      return data.data.success;
    } else {
      return false;
    }
  }

  logIn(appSession: AppSession) {
    const response = this.http.post(`${environment.baseUrl}/sessions/login`, appSession);
    let url: String;
    response.subscribe(
      (data) => {
        // Valida la sesión y dependiendo del estado evaluado retornará la url correspondiente
        url = this.authClass.validSession(data);
        // Independientemente del resultado que arroje la clase AuthClass la información traída de la BD se almacenaŕa en la App.
        ApplicationSettings.setString('data', JSON.stringify(data));
      }, error => {
        console.log('Error Service => ' + error.message);
      }, () => {
        this.redirectTo(url);
      }
    );
  }

  logOut() {
    ApplicationSettings.remove('data');
    this.redirectTo('/login');
  }

  redirectTo(url: String): void {
    this.routerExtensions.navigate([url], {
      transition: {
        name: 'fade'
      },
      clearHistory: true
    });
  }

}
