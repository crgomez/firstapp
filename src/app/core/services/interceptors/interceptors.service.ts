import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApplicationSettings, Dialogs } from '@nativescript/core';
import { RouterExtensions } from '@nativescript/angular';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorsService implements HttpInterceptor {

  constructor(
    private authService: AuthService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const isAuthenticated = this.authService.hasSessionActive();
    let request = req;
    request = req.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    if (ApplicationSettings.getString('data') && isAuthenticated) {
      const dtUser = JSON.parse(ApplicationSettings.getString('data'));
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${dtUser.data.token}`
        }
      });
    }
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        const optionsAlert = {
          title: 'Error de autentificación',
          message: 'Nivel de autentificación: NO SUPERADO\nError 401: Falta un token de autentificación o el token no es válido.',
          okButtonText: 'Aceptar'
        };
        if (err.status === 401) {
          Dialogs.alert(optionsAlert);
        }
        return throwError(err);
      })
    );
  }
}
