import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { Dialogs, ItemEventData, Page } from '@nativescript/core';
import { CourseService } from '@src/app/core/services/courses/course.service';
import { environment } from '@src/environments/environment';
import { Observable } from 'rxjs';
import { Course } from '../../../../core/models/courses';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {
  Courses$: Observable<Course[]>;
  coursesData: Course[];
  isBusy: Boolean = true;
  connectionActive: Boolean;

  constructor(
    private courseService: CourseService,
    private routerExtensions: RouterExtensions,
    private page: Page
  ) {
    if (this.verifyConnectionOnInternet()) {
      this.loadCoursesList();
    } else {
      this.isBusy = false;
    }
  }

  ngOnInit(): void { }

  onTry() {
    if (this.verifyConnectionOnInternet()) {
      this.isBusy = true;
      this.loadCoursesList();
    } else {
      Dialogs.alert('Verifique su conexión a internet');
    }
  }

  goBack(): void {
    this.routerExtensions.navigate(['/home'],
      {
        transition: {
          name: 'fade'
        }
      });
  }

  onTouchFloatButton(): void {
    if (this.verifyConnectionOnInternet()) {
      this.routerExtensions.navigate(['/courses/new'], {
        transition: {
          name: 'fade'
        }
      });
    }
  }

  onItemTap(evt: ItemEventData): void {
    if (this.verifyConnectionOnInternet()) {
      const index = evt.index;
      this.Courses$
        .subscribe(data => {
          const selectedItem = data[index];
          this.openDetails(selectedItem);
        });
    }
  }

  private openDetails(selectedItem: Course) {
    if (this.verifyConnectionOnInternet()) {
      const id = selectedItem['id'];
      this.routerExtensions.navigate(['/courses/details', id], {
        transition: {
          name: 'fade'
        }
      });
    }
  }

  loadCoursesList(): void {
    this.Courses$ = this.courseService.getCourses();
    this.Courses$.subscribe(
      data => {
        this.coursesData = data;
      },
      error => {
        console.log(error);
      },
      () => {
        setTimeout(() => {
          this.isBusy = false;
        }, 500);
      });
  }

  verifyConnectionOnInternet(): Boolean {
    this.connectionActive = environment.connectionActive;
    return this.connectionActive;
  }

  tryConnection(statusConnection: any) {
    this.connectionActive = statusConnection;
    if (this.connectionActive) {
      this.page.actionBarHidden = false;
      this.isBusy = true;
      this.loadCoursesList();
    }
  }

}
