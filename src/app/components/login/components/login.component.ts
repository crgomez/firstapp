import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { Dialogs, ApplicationSettings } from '@nativescript/core';
import { AuthService } from '@src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  isBusy: Boolean = false;

  constructor(
    private routerExtensions: RouterExtensions,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  onNavItemTap(NavItemTap: String) {
    this.routerExtensions.navigate([NavItemTap], {
      transition: {
        name: 'fade'
      }
    });
  }

  logIn() {
    const alertOptions = { title: 'Inicio de sesión', message: 'Matrícula y/o contraseña incorrectos', okButtonText: 'Aceptar' };
    if (this.form.valid) {
      this.authService.logIn(this.form.value);
      let data: any;
      this.isBusy = true;
      setTimeout(() => {
        data = JSON.parse(ApplicationSettings.getString('data'));
        if (data.data.success === false) {
          this.isBusy = false;
          Dialogs.alert(alertOptions);
        }
      }, 500);
    }
  }

  buildForm() {
    this.form = this.formBuilder.group({
      matricule: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      password: ['', [Validators.required]]
    });
  }

  get matriculeField() {
    return this.form.get('matricule');
  }

  get passwordField() {
    return this.form.get('password');
  }

}
