import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  pdfUrl = 'http://bibliotecadigital.ilce.edu.mx/Colecciones/CuentosMas/AventurasPinocho.pdf';

  constructor() { }

  ngOnInit(): void {
  }

  /* onLoad() {
    console.log('Cargando el pdf');
  } */

}
