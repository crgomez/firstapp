import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { PDFViewModule } from 'nativescript-pdf-view/angular';

@NgModule({
  declarations: [],
  imports: [
    NativeScriptCommonModule,
    PDFViewModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PluginsModule { }
